var titulo = document.querySelector(".titulo");
titulo.textContent = "Aparecida Nutricionista"

var pacientes = document.querySelectorAll(".paciente");

for(var i = 0; i < pacientes.length; i++){
    var paciente = pacientes[i];

    var TdPeso = paciente.querySelector(".info-peso");
    var TdAltura = paciente.querySelector(".info-altura");
    var TdImc = paciente.querySelector(".info-imc");
    
    var peso = TdPeso.textContent;
    var altura = TdAltura.textContent; 
    
    var pesoValido = true;
    var alturaValida= true;
    
    if(peso <= 0 || peso >= 1000){
        TdImc.textContent = "Peso inválido !";
        pesoValido = false;
    }else if(altura <= 0 || altura >= 3.00){
        TdImc.textContent = "Altura inválida";
        alturaValida = false;
    }else if(pesoValido == true && alturaValida == true){
        var imc = peso / ( altura * altura);
        TdImc.textContent = imc.toFixed(2);
    }
}




